var indicador1;
var indicador2;

function getPeriodosTrimestre(periodoSeleccionado){

  var arrayPeriodos = [];
  var arrayTrimestre = [];

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
    if(parseInt(keyPeriodo) <= parseInt(periodoSeleccionado)){
      arrayPeriodos.push(keyPeriodo);
    }
  });

  for( i = 0; i < 7; i++){
    if(arrayPeriodos.length > 0){
      arrayTrimestre.unshift(Math.max.apply(Math, arrayPeriodos)+'');
      arrayPeriodos = jQuery.grep(arrayPeriodos, function(value) {
        return value != Math.max.apply(Math, arrayPeriodos);
      });
    }
  }

  return arrayTrimestre;
}


function getPeriodos(){
 var arrayPeriodos = [];

 $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
   arrayPeriodos.push(keyPeriodo);
 });

 return arrayPeriodos;
}

var arrPeriodos = getPeriodos();
var periodoActual = Math.max.apply(Math, arrPeriodos)+'';

$.each(arrPeriodos, function (index, value) {
    var selected = value==periodoActual?'selected':'';
    $('#cmbPeriodo').append('<option value="'+value+'" '+selected+'>'+value+'</option>');
    // $('#cmbPeriodoConteo').append('<option value="'+value+'" '+selected+'>'+value+'</option>');
});

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function resultadosDomiciliacion(ind1,ind2){
  indicador1 = ind1;
  indicador2 = ind2;
  drawChart();
}

function drawChart() {

  var arrayIndicador1 = [['Periodo', 'Monto']];
  var arrayIndicador2 = [['Periodo', 'Conteo']];

  var periodoSelect = $("#cmbPeriodo").val() == null?Math.max.apply(Math, getPeriodos())+'':$("#cmbPeriodo").val();
  // var periodoSelectConteo = $("#cmbPeriodoConteo").val() == null?Math.max.apply(Math, getPeriodos())+'':$("#cmbPeriodoConteo").val();

  var arrPeriodos = getPeriodosTrimestre(periodoSelect);
  // var arrPeriodosConteo = getPeriodosTrimestre(periodoSelectConteo);

  console.log(indicador1);
  console.log(indicador2);

  if(indicador1 != undefined){
    $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
      var txResultado = objPeriodo[indicador1].resultado.txResultado;
      arrayIndicador1.push([keyPeriodo, parseInt(txResultado)]);
    });
  }

  if(indicador2 != undefined){
    $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
      var txResultado = objPeriodo[indicador2].resultado.txResultado;
      arrayIndicador2.push([keyPeriodo, parseInt(txResultado)]);
    });
  }

  arrayIndicador1 = jQuery.grep(arrayIndicador1, function(value,index) {
    return $.inArray(value[0],arrPeriodos) != -1 || index == 0;
  });

  arrayIndicador2 = jQuery.grep(arrayIndicador2, function(value,index) {
     return $.inArray(value[0],arrPeriodos) != -1 || index == 0;
  });

 var data1 = google.visualization.arrayToDataTable(arrayIndicador1);
 var data2 = google.visualization.arrayToDataTable(arrayIndicador2);

 var options = {
   curveType: 'function',
   hAxis:{title: 'Periodos'},
   vAxis:{title: 'Monto del Valor del KPI',format:'$#,###'},
   legend: {position: 'none'}
 };

 var option = {
   curveType: 'function',
   hAxis:{title: 'Periodos'},
   vAxis:{title: 'Conteo del Valor del KPI',format:'#,###'},
   legend: {position: 'none'}
 };
 var chart2 = new google.visualization.LineChart(document.getElementById('curve'));
 chart2.draw(data2, option);

 var chart1 = new google.visualization.LineChart(document.getElementById('curve_chart'));
 chart1.draw(data1, options);

}

$(window).resize(function(){
  drawChart();
});
