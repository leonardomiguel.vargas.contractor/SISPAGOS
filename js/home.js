function loadHome(){
    var wH = $(window).height()-100;
    var wW = $(window).width()-40;
  $('#home figure').css({
    'height':wH,
    'width':wW,
    'text-align':'center'
  });
}

$("[data-toggle=popover]")
.popover({html:true})


loadHome();
loadSISPAGOS_DB();

$(window).resize(function(){
  loadHome();
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
