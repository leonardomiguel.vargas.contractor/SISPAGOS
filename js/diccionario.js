
var arrDiccionario = [];


function loadDiccionario(){
  arrDiccionario = [];
  $.each(DB.diccionario, function (index, value) {
    arrDiccionario.push(value);
  });
  $("#tableDiccionario").bootstrapTable('load', arrDiccionario);
}

_loadDiccionario(loadDiccionario);


function insertarDiccionario(){
  $('#add_diccionario').on('shown.bs.modal', function() {
    $("#formDiccionario").bootstrapValidator('resetForm',true);
  });
  $('#add_diccionario').modal('show');
}


function guardarDiccionario(){
  var objDiccionario = {};
  var cdDiccionario = $('#cdDiccionario').val();
  var nbSeccion     = $('#nbSeccion').val();
  var nbConcepto    = $('#nbConcepto').val();
  var nbAmbiente    = $('#nbAmbiente').val();
  var nbEsquema     = $('#nbEsquema').val();
  var nbTabla       = $('#nbTabla').val();
  var nbCampo       = $('#nbCampo').val();
  var nbDescipcion  = $('#nbDescipcion').val();
  var nbTipoDato    = $('#nbTipoDato').val();

  objDiccionario.cdDiccionario = getSecuencia();

  objDiccionario = {
    'cdDiccionario': objDiccionario.cdDiccionario,
    'nbSeccion': nbSeccion,
    'nbConcepto': nbConcepto,
    'nbAmbiente': nbAmbiente,
    'nbEsquema': nbEsquema,
    'nbTabla': nbTabla,
    'nbCampo': nbCampo,
    'nbDescipcion': nbDescipcion,
    'nbTipoDato': nbTipoDato
  };

  $('#formDiccionario input').each(function(index){
    var input = $(this);
    objDiccionario[input.attr('id')] = input.val();
  });

  DB.diccionario[objDiccionario.cdDiccionario] = objDiccionario;
  setDiccionario(objDiccionario.cdDiccionario, objDiccionario);
}


function getSecuencia() {
  var id = $('#tableDiccionario').bootstrapTable('getData').length+1;
  var bien = id -1;
  return  bien;
}

// function getSecuencia() {
//   var _array = [];
//   var idTabla = $('#tableDiccionario').bootstrapTable('getData').length+1;
//   $.each(arrDiccionario, function (index, objDicc) {
//     _array.push(objDicc.cdDiccionario);
//   });
//   var id = (Math.max.apply(Math,_array))+2;
//   return id;
// }

function setDiccionario(cdDiccionario,row){
  $("body").addClass("loading");
  database.ref('diccionario/'+cdDiccionario+'/').set(row);
  $("body").removeClass("loading");
  loadDiccionario();
}
var tabla = $('#tableDiccionario');
$('#tableDiccionario').bootstrapTable({
  search: true,
  pagination: true,
  pageSize: 10,
  pageList: [10, 15],
  mobileResponsive:true,
  data : arrDiccionario,

  columns: [{
    field: 'cdDiccionario',
    title: 'ID Diccionario',
    sortable: true,
    visible: false
  },{
    field: 'nbSeccion',
    title: 'Sección',
    sortable: true
  },{
    field: 'nbConcepto',
    title: 'Concepto',
    sortable: true
  },{
    field: 'nbAmbiente',
    title: 'Ambiente',
    sortable: true
  },{
    field: 'nbEsquema',
    title: 'Esquema',
    sortable: true
  },{
    field: 'nbTabla',
    title: 'Tabla',
    sortable: true
  },{
    field: 'nbCampo',
    title: 'Campo',
    sortable: true
  },{
    field: 'nbDescipcion',
    title: 'Descripcion',
    sortable: true
  },{
    field: 'nbTipoDato',
    title: 'Tipo de Dato',
    sortable: true
  }],
  exportDataType: 'all',
  exportTypes:['excel'],
  exportOptions:{
    fileName:'Diccionario de Datos',
    worksheetName:'Diccionario de Datos'
  },
  uniqueId: 'cdDiccionario',

  formatShowingRows: function (pageFrom, pageTo, totalRows) {
    return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
  },
  formatRecordsPerPage: function (pageNumber) {
  	return pageNumber + ' registros por pagina';
  },
  formatLoadingMessage: function () {
  	return 'Cargando, espere por favor...';
  },
  formatSearch: function () {
  	return 'Buscar';
  },
  formatNoMatches: function () {
  	return 'No se encontro informaci&oacute;n';
  }

});

$("#diccionario .search").addClass("input-group");
$("#btnInsertarDiccionario").attr('disabled','disabled');
$("#formDiccionario").bootstrapValidator({

  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {

      nbSeccion: {
          validators: {
            notEmpty: {
              message: 'El campo Sección es requerido.'
            }
          }
      },
      nbConcepto: {
          validators: {
            notEmpty: {
              message: 'El campo Concepto es requerido.'
            }
          }
      },
      nbAmbiente: {
          validators: {
            notEmpty: {
              message: 'El campo Ambiente es requerido.'
            }
        }
      },
      nbEsquema: {
          validators: {
            notEmpty: {
              message: 'El campo Esquema es requerido.'
            }
        }
      },
      nbTabla: {
          validators: {
            notEmpty: {
              message: 'El campo Tabla es requerido.'
            }
          }
      },
      nbCampo: {
          validators: {
            notEmpty: {
              message: 'El campo llamado Campo es requerido.'
            }
          }
      },
      nbDescipcion: {
          validators: {
            notEmpty: {
              message: 'El campo Descripción es requerido.'
            }
          }
      },
      nbTipoDato: {
          validators: {
            notEmpty: {
              message: 'El campo Tipo de Dato es requerido.'
            }
          }
      }

    }
}).on('status.field.bv', function(e, data) {
  if(!$("#formDiccionario").data('bootstrapValidator').isValid()){
    $("#btnInsertarDiccionario").attr('disabled','disabled');
  }else{
    $("#btnInsertarDiccionario").removeAttr('disabled');
  }
});
