$("#divBusquedaAvanzada").hide();

var arrayHeader_indicadoresATM = [];
var arrayDataTable_indicadoresATM = [];
var chart_indicadoresATM;
var data_indicadoresATM;
var options_indicadoresATM;

function getPeriodosTrimestre(periodoSeleccionado){

  var arrayPeriodos = [];
  var arrayTrimestre = [];

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
    if(parseInt(keyPeriodo) <= parseInt(periodoSeleccionado)){
      arrayPeriodos.push(keyPeriodo);
    }
  });

  for( i = 0; i < 7; i++){
    if(arrayPeriodos.length > 0){
      arrayTrimestre.unshift(Math.max.apply(Math, arrayPeriodos)+'');
      arrayPeriodos = jQuery.grep(arrayPeriodos, function(value) {
        return value != Math.max.apply(Math, arrayPeriodos);
      });
    }
  }

  return arrayTrimestre;
}

function getPeriodosATM(){
 var arrayPeriodos = [];

 $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
   arrayPeriodos.push(keyPeriodo);
 });

 return arrayPeriodos;
}

function getPuntuacionPeriodoEntidad(periodo,entidad){
  var puntuacion = 0;
  var nbIndicador = $("#cmbIndicadores").val();
  $.each(DB.estados, function (keyEntidad, objEntidad) {
      if(objEntidad.edo == entidad){
        $.each(objEntidad.indicadores, function (keyIndicador, objIndicador) {
          if(keyIndicador == nbIndicador){
            $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
                if(keyPeriodo == periodo){
                  if(objPeriodo[objIndicador] != undefined){
                    if(objPeriodo[objIndicador].resultado != undefined){
                      puntuacion = parseFloat(objPeriodo[objIndicador].resultado.txResultado);
                    }
                  }
                  return puntuacion;
                }
            });
          }
        });
      }
  });
  return puntuacion;
}

google.charts.load('current', {'packages':['corechart','bar']});
google.charts.setOnLoadCallback(draw_indicadoresATM);

function carga_indicadoresATM(){

  var entidadSelect = $("#cmbEntidadFederativa").val();
  var periodoSelect = $("#cmbPeriodo").val() == null?Math.max.apply(Math, getPeriodosATM())+'':$("#cmbPeriodo").val();

  arrayHeader_indicadoresATM.push('Periodo');
  $.each(DB.estados, function (key, obj) {
      var entidad = obj.edo;
      if(entidadSelect == "-1"){
          arrayHeader_indicadoresATM.push(entidad);
      }else{
          arrayHeader_indicadoresATM.push(entidadSelect);
          return false;
      }
  });

  arrayDataTable_indicadoresATM.push(arrayHeader_indicadoresATM);

  var arrPeriodos = getPeriodosTrimestre(periodoSelect);

  $.each(arrPeriodos, function (index, value) {
      var rowPeriodo = [];
      var periodo = value;

        rowPeriodo.push(periodo);

        $.each(DB.estados, function (key, obj) {
            var estado = obj.edo;
            if(entidadSelect == "-1"){
                rowPeriodo.push(getPuntuacionPeriodoEntidad(periodo,estado));
            }else{
                rowPeriodo.push(getPuntuacionPeriodoEntidad(periodo,entidadSelect));
                return false;
            }
        });
      arrayDataTable_indicadoresATM.push(rowPeriodo);
  });

  data_indicadoresATM = google.visualization.arrayToDataTable(arrayDataTable_indicadoresATM);

  options_indicadoresATM = {
    vAxis: {title: 'Puntuación'},
    hAxis: {title: 'Periodo'},
    seriesType: 'bars',
    colors: [
      'rgb(137, 209, 243)',
      'rgb(0, 158, 229)',
      'rgb(9, 79, 164)',
      'rgb(134, 200, 45)',
      'rgb(253, 189, 44)',
      'rgb(246, 137, 30)',
      'rgb(179, 179, 179)',
      'rgb(183, 194, 4)',
      'rgb(0, 110, 193)',
      'rgb(62, 182, 187)'
    ]
  };
}

function draw_indicadoresATM() {

  carga_indicadoresATM();

  chart_indicadoresATM = new google.visualization.ColumnChart(document.getElementById('chart_indicadoresATM'));
  chart_indicadoresATM.draw(data_indicadoresATM, options_indicadoresATM);

 }

 function actualizar_indicadoresATM(){
   arrayDataTable_indicadoresATM = [];
   arrayHeader_indicadoresATM = [];
   carga_indicadoresATM();
   chart_indicadoresATM.draw(data_indicadoresATM, options_indicadoresATM);
 }

function toggleBusquedaAvanzada(){
  if($("#divBusquedaAvanzada").is(":visible")){
    $("#divBusquedaAvanzada").hide();
    $('#cmbEntidadFederativa').empty();
    $('#cmbEntidadFederativa').append('<option value="-1" selected>Todas las Entidades</option>');
    $('#cmbPeriodo').empty();
    selectEntidadMapa("-1");
    actualizar_indicadoresATM();
    drawRegionsMap();
  }else{
    $("#divBusquedaAvanzada").show();

    $.each(DB.estados, function (key, obj) {
        $('#cmbEntidadFederativa').append('<option value="'+obj.edo+'">'+obj.name+'</option>');
    });

    var arrPeriodos = getPeriodosATM();

    var periodoActual = Math.max.apply(Math, arrPeriodos)+'';

    $.each(arrPeriodos, function (index, value) {
        var selected = value==periodoActual?'selected':'';
        $('#cmbPeriodo').append('<option value="'+value+'" '+selected+'>'+value+'</option>');
    });
  }
}

function selectEntidadMapa(entidad){
  $.each(DB.estados, function (key, obj) {
      if(obj.edo == entidad.value){
        //document.getElementById("e_"+obj.edo).style="fill:rgb(0, 110, 193) !important";
      }else{
        //document.getElementById("e_"+obj.edo).style="";
      }
  });
}

function loadEventMap(){
    $.each(DB.estados, function (key, obj) {
      document.getElementById("e_"+obj.edo).onclick = function() {
        document.getElementById("cmbEntidadFederativa").value = obj.edo;
        actualizar_indicadoresATM();
        var entidad = {};
        entidad.value = obj.edo;
        selectEntidadMapa(entidad);
      };
    });
}

$(window).resize(function(){
  actualizar_indicadoresATM();
});
