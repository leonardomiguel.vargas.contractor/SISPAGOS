function getPeriodosMapa(periodoActual){

  var arrPeriodosMapa = [];
  var arrayPeriodos = [];

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
    arrayPeriodos.push(keyPeriodo);
  });

  var verificaMes;
  var inmdtAnte;
  var actual;

  if(periodoActual != ""){
    actual = periodoActual;
  }else{
    actual = Math.max.apply(Math, arrayPeriodos)+'';
  }

  verificaMes = actual.substr(actual.length - 2);
  verificaMes = parseInt(verificaMes);
  actual = parseInt(actual);

  if(verificaMes == 01){
    inmdtAnte = actual-89;
  }else{
    inmdtAnte = actual-1;
  }

  arrPeriodosMapa.push(actual);
  arrPeriodosMapa.push(inmdtAnte);

  return arrPeriodosMapa;
}

function getResultadoEntidadIndicadorPeriodo(entidad,indicador,periodo){
  var puntuacion = 0;
  $.each(DB.estados, function (keyEntidad, objEntidad) {
      if(keyEntidad == entidad){
        $.each(objEntidad.indicadores, function (keyIndicador, objIndicador) {
          if(keyIndicador == indicador){
            $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
                if(keyPeriodo == periodo){
                  if(objPeriodo[objIndicador] != undefined){
                    if(objPeriodo[objIndicador].resultado != undefined){
                      puntuacion = parseFloat(objPeriodo[objIndicador].resultado.txResultado);
                    }
                  }
                  return puntuacion;
                }
            });
          }
        });
      }
  });
  return puntuacion;
}

function calcularDesviacion(){
  var periodoSeleccionado = $("#cmbPeriodo").val()==null?"":$("#cmbPeriodo").val();
  var indicadorSeleccionado = $("#cmbIndicadores").val();
  var periodosMapa = getPeriodosMapa(periodoSeleccionado);

  $.each(DB.estados, function (keyEstado, objEstado) {

    var resultadoPeriodoActual = getResultadoEntidadIndicadorPeriodo(keyEstado,indicadorSeleccionado,periodosMapa[0]);
    var resultadoPeriodoAnterior = getResultadoEntidadIndicadorPeriodo(keyEstado,indicadorSeleccionado,periodosMapa[1]);
    var desviacion = 0;
    if(resultadoPeriodoActual <= 0){
      desviacion = -1;
    }else{
      desviacion = Math.abs((resultadoPeriodoActual-resultadoPeriodoAnterior)/resultadoPeriodoActual*100);
    }

    DB.estados[keyEstado]['desviacion'] = desviacion;

  });
}

google.charts.load('visualization', '1', {'packages': ['geochart']});
google.charts.setOnLoadCallback(drawRegionsMap);

function drawRegionsMap() {

  calcularDesviacion();

  var mapa_tGET = $.map(DB.estados, function(el, i) {
    return [[el.edo,el.name, el.desviacion]];
  });

  var data = new google.visualization.DataTable();

  data.addColumn('string', 'ID');
  data.addColumn('string', 'Estado');
  data.addColumn('number', 'Desviacion');
  data.addRows(
    mapa_tGET
  );

  var options = {
    region: 'MX',
    resolution: 'provinces',
    title:"Republica Mexicana",
    colorAxis: {
      values:[-1,0,5,100],
      colors: [
       'rgb(179, 179, 179)',
       'rgb(0,255,0)',
       'rgb(231, 132, 3)',
       'rgb(243, 61, 16)'
      ]
    },
    backgroundColor: '#81d4fa',
    datalessRegionColor: '#FFFFFF',
    defaultColor: '#FFFFFF'
  };

  var chart = new google.visualization.GeoChart(document.getElementById("mapa"));
  chart.draw(data, options);
};


$(window).resize(function(){
  drawRegionsMap();
});
