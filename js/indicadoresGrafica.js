var indicadorSeleccionado = $("#cmbIndicadores").val();

function getPeriodosTrimestre(periodoSeleccionado){

  var arrayPeriodos = [];
  var arrayTrimestre = [];

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
    if(parseInt(keyPeriodo) <= parseInt(periodoSeleccionado)){
      arrayPeriodos.push(keyPeriodo);
    }
  });

  for( i = 0; i < 7; i++){
    if(arrayPeriodos.length > 0){
      arrayTrimestre.unshift(Math.max.apply(Math, arrayPeriodos)+'');
      arrayPeriodos = jQuery.grep(arrayPeriodos, function(value) {
        return value != Math.max.apply(Math, arrayPeriodos);
      });
    }
  }

  return arrayTrimestre;
}

function getPeriodos(){
 var arrayPeriodos = [];

 $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
   arrayPeriodos.push(keyPeriodo);
 });

 return arrayPeriodos;
}

var arrPeriodos = getPeriodos();
var periodoActual = Math.max.apply(Math, arrPeriodos)+'';

$.each(arrPeriodos, function (index, value) {
    var selected = value==periodoActual?'selected':'';
    $('#cmbPeriodo').append('<option value="'+value+'" '+selected+'>'+value+'</option>');
});

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var arrayIndicadores = [['Periodo', 'Periodo',{type: 'string', role: 'tooltip','p': {'html': true}},'Periodo(-12)',{type: 'string', role: 'tooltip','p': {'html': true}}]];
  var periodoSelect = $("#cmbPeriodo").val() == null?Math.max.apply(Math, getPeriodosATM())+'':$("#cmbPeriodo").val();

 var arrayTrimestre = getPeriodosTrimestre(periodoSelect);

 var arrayValuesPeriodo1 = [];
 var arrayValuesPeriodo2 = [];
 $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
   var txResultadoActual = objPeriodo[indicadorSeleccionado].resultado.txResultado;
   var keyPeriodoAnterior = (parseInt(keyPeriodo.substring(0,4))-1+'')+keyPeriodo.substring(4,6);
   var txResultadoAnterior = 0;
   try{
     txResultadoAnterior = DB.parametria[keyPeriodoAnterior][indicadorSeleccionado].resultado.txResultado;
   }catch(e){
     console.log(e);
   }
   arrayIndicadores.push([keyPeriodo, parseInt(txResultadoActual),'<div style="padding:10px;font-size:15px;"><b>'+keyPeriodo+'</b><p></p>Cantidad: <b>'+parseInt(txResultadoActual)+'</b></div>', parseInt(txResultadoAnterior),'<div style="padding:10px;font-size:15px;"><b>'+keyPeriodoAnterior+'</b><p></p>Cantidad: <b>'+parseInt(txResultadoAnterior)+'</b></div>']);

 });

 arrayIndicadores = jQuery.grep(arrayIndicadores, function(value,index) {
     return $.inArray(value[0],arrayTrimestre) != -1 || index == 0;
 });

 $.each(arrayIndicadores, function (index, value) {
   if(index != 0 ){
     arrayValuesPeriodo1.push(value[1]);
     arrayValuesPeriodo2.push(value[3]);
   }
 });

 var maxValuePeriodo1 = Math.max.apply(Math,arrayValuesPeriodo1);
 var maxValuePeriodo2 = Math.max.apply(Math,arrayValuesPeriodo2);
 var maxValue = 0;

console.log(maxValuePeriodo1);
console.log(maxValuePeriodo2);

 if(maxValuePeriodo1 > maxValuePeriodo2){
   maxValue = maxValuePeriodo1;
 }else{
   maxValue = maxValuePeriodo2;
 }

 console.log(maxValue);

 $.each(arrayIndicadores, function (index, value) {
   if(index != 0){
     var valorPeriodoActual = value[1];
     var valorPeriodoAnterior = value[3];
     arrayIndicadores[index][1] = ((valorPeriodoActual * 100) / maxValue);
     arrayIndicadores[index][3] = ((valorPeriodoAnterior * 100) / maxValue);
   }
 });

 var data = google.visualization.arrayToDataTable(arrayIndicadores);


 var options = {
  hAxis:{title: 'Periodos'},
  vAxis:{
    title: 'Porcentaje del Valor del KPI',
    viewWindow: {
        min: 0,
        max: 100
    },
    ticks: [{v:0, f:'0%'},{v:10, f:'10%'},{v:20, f:'20%'},{v:30, f:'30%'},{v:40, f:'40%'},{v:50, f:'50%'},{v:60, f:'60%'},{v:70, f:'70%'},{v:80, f:'80%'},{v:90, f:'90%'},{v:100, f:'100%'}]
  },
  seriesType: 'bars',
  series: {
    1: {type: 'line',visibleInLegend: false}
  },
  legend: {position: 'none'},
  tooltip: { isHtml: true },
  pointSize: 10,
  colors: [
    'rgb(137, 209, 243)',
    'rgb(0, 158, 229)',
    'rgb(9, 79, 164)',
    'rgb(134, 200, 45)',
    'rgb(253, 189, 44)',
    'rgb(246, 137, 30)',
    'rgb(179, 179, 179)',
    'rgb(183, 194, 4)',
    'rgb(0, 110, 193)',
    'rgb(62, 182, 187)'
  ]
 };

 var chart = new google.visualization.ColumnChart(document.getElementById('curve_chart'));
 chart.draw(data, options);
}

$(window).resize(function(){
  drawChart();
});
