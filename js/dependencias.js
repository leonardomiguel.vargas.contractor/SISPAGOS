function getPeriodos(){
  var arrayPeriodos = [];

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
    arrayPeriodos.push(keyPeriodo);
  });

  return arrayPeriodos;
}

var arrPeriodos = getPeriodos();
var periodoActual = Math.max.apply(Math, arrPeriodos)+'';

$.each(arrPeriodos, function (index, value) {
    var selected = value==periodoActual?'selected':'';
    $('#cmbPeriodo').append('<option value="'+value+'" '+selected+'>'+value+'</option>');
});

function loadDependencias(){
  $('#listTable').empty();

  $.each(DB.relacionTablas, function (keyTable, objTable) {

    if(objTable.nbEsquema == "FACT"){

      var arrayTablePeriodoWarning = [];
      var TABLA = keyTable;
      var COMENTARIOS = objTable.txComentarios==""?"SIN COMENTARIOS":objTable.txComentarios;

      var rowTable = '';
      rowTable = '<div class="panel panel-default">'
                  +'<div class="panel-heading">'
                    +'<div data-parent="#listTable" data-toggle="collapse" href="#'+TABLA+'" style="cursor:pointer;">'
                      +'<ul class="nav nav-pills pull-right" style="margin:5px;">';

                      $.each(getIndicadoresTable(objTable.cdValidacion), function (index, indicadores) {
                          var periodo = indicadores.periodo;
                          var txResultado = indicadores.txResultado == undefined?0:indicadores.txResultado;
                          var fhEjecucion = indicadores.fhEjecucion;
                          rowTable += '<li><button id="'+TABLA+'_'+periodo+'" class="btn btn-'+semaforoTable(txResultado)+'" type="button"> '+periodo+' <span class="badge pull-right" style="margin:2px 0px 0px 5px;">'+txResultado+'</span></button></li>';
                          // rowTable += '<li><div class="btn-group" role="group"><button class="btn btn-default" type="button"> '+periodo+' </button><button id="'+TABLA+'_'+periodo+'" type="button" class="btn btn-'+semaforoTable(txResultado)+'"><span class="glyphicon glyphicon-ok pull-right" style="font-size:20px;" data-toggle="tooltip" title="'+txResultado+'"></span></button></div></li>';
                      });

                      rowTable += '</ul>'
                      +'<h4 class="list-group-item-heading">'+TABLA+'</h4>'
                      +'<p class="list-group-item-text">'+COMENTARIOS+'</p>'
                    +'</div>'
                  +'</div>'
                  +'<div class="panel-collapse collapse" id="'+TABLA+'">'
                    +'<ul class="list-group">';
                      $.each(objTable.dependencias, function (index, dependencia) {

                        var TABLA_DEPENDENCIA = $.trim(dependencia);
                        var objDependencia = getObjTable(TABLA_DEPENDENCIA);
                        var COMENTARIOS_DEPENDENCIA = objDependencia.txComentarios=="" || objDependencia.txComentarios == undefined?"SIN COMENTARIOS":objDependencia.txComentarios;
                        var VALIDACION_DEPENDENCIA = objDependencia.cdValidacion;
                      rowTable += '<a href="#" class="list-group-item" style="padding-left:19px;">'
                                    +'<ul class="nav nav-pills pull-right" style="margin:5px;">';

                                    $.each(getIndicadoresTable(VALIDACION_DEPENDENCIA), function (index, indicadores) {
                                      var periodo = indicadores.periodo;
                                      var txResultado = indicadores.txResultado == undefined?0:indicadores.txResultado;
                                      var fhEjecucion = indicadores.fhEjecucion;
                                      var semaforo = semaforoTable(txResultado);
                                      rowTable += '<li><button class="btn btn-'+semaforo+'" type="button"> '+periodo+' <span class="badge pull-right" style="margin:2px 0px 0px 5px;">'+txResultado+'</span></button></li>';
                                      // rowTable += '<li><div class="btn-group" role="group"><button class="btn btn-default" type="button"> '+periodo+' </button><button type="button" class="btn btn-'+semaforoTable(txResultado)+'"><span class="glyphicon glyphicon-exclamation-sign pull-right" style="font-size:20px;" title="'+txResultado+'"></span></button></div></li>';
                                      if(semaforo == "danger"){
                                        arrayTablePeriodoWarning.push(TABLA+"_"+periodo);
                                      }
                                    });

                                    rowTable += '</ul>'
                                    +'<h5 class="list-group-item-heading"><b>'+TABLA_DEPENDENCIA+'</b></h5>'
                                    +'<p class="list-group-item-text">'+COMENTARIOS_DEPENDENCIA+'</p>'
                                  +'</a>';
                      });
                     rowTable += '</ul>';

      rowTable += '</div></div>';

      $('#listTable').append(rowTable);

      $.each(arrayTablePeriodoWarning, function (index, tablePeriodoWarning) {
        $("#"+tablePeriodoWarning).removeClass("btn-danger");
        $("#"+tablePeriodoWarning).removeClass("btn-success");
        $("#"+tablePeriodoWarning).addClass("btn-warning");
      });

    }
  });
}


function getObjTable(tableName){
  var table = {};
  $.each(DB.relacionTablas, function (keyTable, objTable) {
      if(keyTable == tableName){
        table = objTable;
      }
  });
  return table;
}

function getIndicadoresTable(cdValidacion){
  var arrPeriodos = [];
  var arrPeriodoFiltro = [];
  var objIndicador = {};

 var periodoSelect = $("#cmbPeriodo").val() == null?Math.max.apply(Math, getPeriodos())+'':$("#cmbPeriodo").val();

  $.each(DB.parametria, function (keyPeriodo, objPeriodo) {
      objIndicador = {};
      if(parseInt(keyPeriodo) <= parseInt(periodoSelect)){
        objIndicador.periodo = keyPeriodo;
        arrPeriodoFiltro.push(keyPeriodo);
        $.each(objPeriodo, function (keyValidacion, objValidacion) {
            if(keyValidacion == cdValidacion){
              if(objValidacion.resultado != undefined){
                objIndicador.txResultado = objValidacion.resultado.txResultado==undefined?"":objValidacion.resultado.txResultado;
                objIndicador.fhEjecucion = objValidacion.resultado.fhEjecucion;
              }
              return false;
            }
        });
        arrPeriodos.push(objIndicador);
      }
  });

  var arrPeriodosTemp = [];

  for( i = 0; i < 3; i++){
    if(arrPeriodoFiltro.length > 0){
      arrPeriodoFiltro = jQuery.grep(arrPeriodoFiltro, function(value) {
        if(value == Math.max.apply(Math, arrPeriodoFiltro)){
          arrPeriodosTemp.push(value);
        }
        return value != Math.max.apply(Math, arrPeriodoFiltro);
      });
    }
  }

  arrPeriodos = jQuery.grep(arrPeriodos, function(value) {
    if(jQuery.inArray(value.periodo, arrPeriodosTemp) != -1){
        return true;
    }else{
        return false;
    }
  });

  return arrPeriodos;
}

function semaforoTable(resultado){
    if(resultado > 0){
      return 'success';
    }else if(resultado <= 0){
      return 'danger';
    }
}

loadDependencias();

$(".panel").on("show.bs.collapse hide.bs.collapse", function(e) {
  if (e.type=='show'){
    $(this).addClass('panel-primary');
  }else{
    $(this).removeClass('panel-primary');
  }
});
