// SISPAGOS QA
var config = {
   apiKey: "AIzaSyCEdbqoHgSOkD7eI7gZwWtw9zbJmRakU_0",
   authDomain: "sispagos-qa.firebaseapp.com",
   databaseURL: "https://sispagos-qa.firebaseio.com",
   projectId: "sispagos-qa",
   storageBucket: "sispagos-qa.appspot.com",
   messagingSenderId: "223095634667"
 };

firebase.initializeApp(config);

var provider = new firebase.auth.GoogleAuthProvider();

  firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var usuario = user.toJSON();
    _getUsuario(usuario.email,function(objUsuario){
      if(validarCorreo(usuario.email,objUsuario)){
        $("body").show();
        $("#avatarSesion").css('background-image','url("'+usuario.providerData[0].photoURL+'")');
      }else{
        $("body").html("Usuario invalido.");
        $("body").show();
      }
    });
  }else {
    firebase.auth().signInWithRedirect(provider);
  }
});

var database = firebase.database();
var storage  = firebase.storage();

var parametria  = database.ref('parametria');
var usuarios    = database.ref('usuarios');
var diccionario = database.ref('diccionario');
var storageRef = storage.ref();
