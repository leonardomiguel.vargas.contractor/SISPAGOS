var DB = {};
DB = {
	"parametria": {},
	"parametriaDB": {},
	"usuarios": {},
	"diccionario":{},
	"secciones": {
			"CUENTAS DE NIVEL 4": {
				"rfp": 37,
				"CUENTAS A LA VISTA ASOCIADAS A NÓMINA": [
					"Carga la información de la STG de Cuentas relacionadas a nomina|169",
					"Corte Mensual|235"
				],
				"CUENTAS A LA VISTA NIVEL 4 EN M.N.": [
					"CUENTAS VISTA|275_276",
					"CHEQUES|277_278",
					"DEVOLUCIONES EN CHEQUE|279"
				]
			},
			"TARJETAS": {
				"rfp": 513,
				"DISTRIBUCIÓN DE CAJEROS": [
					"NÚMERO TOTAL DE CAJEROS REMOTOS",
					"NÚMERO TOTAL DE CAJEROS POR SUCURSAL",
					"TOTAL DE NUMERO DE CAJEROS EN ESTADO"
				],
				"DISTRIBUCIÓN TPVS": [
					"NÚMERO TOTAL DE TPVS"
				],
				"MEDIOS DE PAGO": [
					"TARJETAS EMITIDAS|260",
					"TARJETAS UTILIZADAS|261",
					"OPERACIONES CON TARJETAS|262_263",
					"COMERCIO|264",
					"CUENTAS RELACIONADAS A TARJETAS|265_266",
					"TARJETAS DE CREDITO|267",
					"CLIENTES|268_269",
					"PAGOS CON TARJETAS|270_271"
				],
				"OPERACIONES EN CAJEROS AUTOMÁTICOS": [
					"Operaciones en Cajeros ODS|164",
					"Carga del archivo plano SSPCUATRILLAVES|170",
					"Corte mensual|230"
				]
			},
			"SISTEMAS Y MEDIOS PARA TRANSFERENCIAS UTILIZADOS": {
				"rfp": 12,
				"BANCA ELECTRÓNICA": [
					"USUARIOS DE BANCA ELECTRÓNICA|272",
					"FUNCIONES DE BANCA ELECTRÓNICA|273_274"
				],
				"DOMICILIACIONES": [
					/*"Suma de importe con filtro: periodo 201706",
					"Suma de importe con nivel de riesgo 1",
					"Suma de importe con nivel de riesgo 2",
					"Suma de importe con nivel de riesgo 3",
					"Suma de importe con nivel de riesgo 4",
					"Conteo de domiciliación con nivel de riesgo 1",
					"Conteo de domiciliación con nivel de riesgo 2",
					"Conteo de domiciliación con nivel de riesgo 3",
					"Conteo de domiciliación con nivel de riesgo 4",
					"Suma de importe con nivel de riesgo @",
					"Conteo de domiciliación con nivel de riesgo @",*/
					"INTERNAS (DESDE CUENTAS DEL MISMO BANCO) A CUENTAS NIVEL 4/PERSONAS FÍSICAS |131_145",
					"INTERNAS (DESDE CUENTAS DEL MISMO BANCO) A CUENTAS NIVEL 4/PERSONAS MORALES |131_145",
					"INTERBANCARIAS (INICIADAS POR CLIENTES DE OTROS BANCOS) A CUENTAS NIVEL 4/PERSONAS FÍSICAS |131_145",
					"INTERBANCARIAS (INICIADAS POR CLIENTES DE OTROS BANCOS) A CUENTAS NIVEL 4/PERSONAS MORALES |131_145"
					/*"Suma de importe con filtro: tipo_operacion I",
					"conteo de domiciliacion con filtro: periodo 201706",
					"Conteo de domiciliacion:personalidad M",
					"Conteo de domiciliacion:personalidad F",
					"Conteo de domiciliacion: tipo_operacion I",
					"suma de importe con filtros: periodo 201706 personalidad M ",
					"Suma de importe con filtros: periodo 201706 personalidad F",
					"Suma de importe con filtros: periodo 201706 tipo operación I",
					"Conteo de domiciliacion: periodo 201706 personalidad M",
					"Conteo de domiciliacion: periodo 201706 personalidad F",
					"Conteo de domiciliacion: periodo 201706 tipo operación I",
					"Conteo de domiciliacion: periodo 201706 nivel de riesgo 1",
					"Conteo de domiciliacion: periodo 201706 nivel de riesgo 2",
					"Conteo de domiciliacion: periodo 201706 nivel de riesgo 3",
					"Conteo de domiciliacion: periodo 201706 nivel de riesgo 4",
					"Suma de importe con filtros: periodo 201706 nivel de riesgo tipo 1",
					"Suma de importe con filtros: periodo 201706 nivel de riesgo tipo 2",
					"Suma de importe con filtros: periodo 201706 nivel de riesgo tipo 3",
					"Suma de importe con filtros: periodo 201706 nivel de riesgo tipo 4",
					"Contiene la información para Domiciliaciones",
					"Contiene los acumulados de Domiciliaciones"*/
				],
				"TRANSFERENCIAS SWIFT": [
					"TRANSFERENCIAS SWIFT|280_281"
				]
			},
			"MEDIOS DE ACCESO A DISPOSICIÓN DEL CLIENTE": {
				"rfp": 81,
				"BANCA ELECTRÓNICA": 29,
				"HOST TO HOST": 10
			},
			"CHEQUES": {
				"rfp": 10,
				"CUENTAS A LA VISTA NIVEL 4 EN M.N.": 38
			},
			"FRAUDES": {
				"rfp": 27,
				"FRAUDES": 26
			},
			"OPERACIONES EN SUCURSAL": {
				"rfp": 12,
				"OPERACIONES EN SUCURSAL": 12
			}
	},
	"relacionTablas": {
		"TDRR332_DOMICILIA": {
			"cdValidacion": 163,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR332_DOMICILIA",
			"txComentarios": "Contiene la información para Domiciliaciones",
			"dependencias": ["TDRR182_DOMICILIA"]
		},
		"TDRR333_OPER_ATM": {
			"cdValidacion": 164,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR333_OPER_ATM",
			"txComentarios": "Contiene la información para Operaciones en Cajeros en ODS",
			"dependencias": ["TDRR107_OPER_ATM"]
		},
		"TDRR348_MP_EMITIDA": {
			"cdValidacion": 165,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR348_MP_EMITIDA",
			"txComentarios": "Carga la informacion acumulada de las tarjetas emitidas",
			"dependencias": ["TDRR184_MP_TARJ_EMI"]
		},
		"TDRR349_MP_UTILIZADA": {
			"cdValidacion": 166,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR349_MP_UTILIZADA",
			"txComentarios": "Carga la información acumulada de las tarjetas utilizadas",
			"dependencias": ["TDRR186_MP_TARJ_UTIL"]
		},
		"TDRR350_MP_COMERCIO": {
			"cdValidacion": 167,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR350_MP_COMERCIO",
			"txComentarios": "Carga la información acumulada de los Comercios",
			"dependencias": ["TDRR193_COMERCIO"]
		},
		"TDRR353_CUENTA_VISTA": {
			"cdValidacion": 168,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR353_CUENTA_VISTA",
			"txComentarios": "Carga la información de la STG de cuetas a la vista en moneda nacional",
			"dependencias": ["TDRR191_CUENTA_VISTA"]
		},
		"TDRR354_CUENTA_NOMIN": {
			"cdValidacion": 169,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR354_CUENTA_NOMIN",
			"txComentarios": "Carga la información de la STG de Cuentas relacionadas a nomina",
			"dependencias": ["TDRR192_CUENTA_nómina"]
		},
		"TDRR358_CUATRILLAVE": {
			"cdValidacion": 170,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR358_CUATRILLAVE",
			"txComentarios": "Carga del archivo plano SSPCUATRILLAVES.txt",
			"dependencias": []
		},
		"TDRR359_TALONARIO": {
			"cdValidacion": 171,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR359_TALONARIO",
			"txComentarios": "Carga la información STG de Talonarios",
			"dependencias": ["TDRR197_TALONARIO"]
		},
		"TDRR360_EMI_CHQ_MXN": {
			"cdValidacion": 172,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR360_EMI_CHQ_MXN",
			"txComentarios": "Carga STG de Cheques MXN",
			"dependencias": ["TDRR701_EMI_CHQ_MXN"]
		},
		"TDRR361_CHQ_OBANCO": {
			"cdValidacion": 173,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR361_CHQ_OBANCO",
			"txComentarios": "Carga de información STG de cheques",
			"dependencias": ["TDRR702_EMI_CHQ_USD ", " TDRR703_DEV_CHQ_MXN ", " TDRR704_DEV_CHQ_USD"]
		},
		"TDRR364_USU_AST": {
			"cdValidacion": 249,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR364_USU_AST",
			"txComentarios": "Carga de información USU_AST",
			"dependencias": []
		},
		"TDRR365_FUN_AST": {
			"cdValidacion": 175,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR365_FUN_AST",
			"txComentarios": "Carga de información STG de Funciones",
			"dependencias": ["TDRR200_FUN_AST"]
		},
		"TDRR363_LOG_AST": {
			"cdValidacion": 176,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR363_LOG_AST",
			"txComentarios": "Carga de información STG de LOGAST",
			"dependencias": ["TDRR198_LOG_AST"]
		},
		"TDRR367_TRANS_SWIFT": {
			"cdValidacion": 177,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR367_TRANS_SWIFT",
			"txComentarios": "Carga la información de Transferencias Swift",
			"dependencias": ["TDRR708_TRANS_SWIFT"]
		},
		"TDRR851_TPV_VERTICAL": {
			"cdValidacion": 178,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR851_TPV_VERTICAL",
			"txComentarios": "CONTIENE LOS ACUMULADOS DE TERMINALES PUNTO DE VENTA",
			"dependencias": ["TDRR282_GEOLOCALIZA"]
		},
		"TDRR852_ATM_VERTICAL": {
			"cdValidacion": 179,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR852_ATM_VERTICAL",
			"txComentarios": "CONTIENE LOS ACUMULADOS DE CAJEROS AUTOMÁTICOS",
			"dependencias": ["TDRR282_GEOLOCALIZA"]
		},
		"TDRR853_DOM_VERTICAL": {
			"cdValidacion": 180,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR853_DOM_VERTICAL",
			"txComentarios": "CONTIENE LOS ACUMULADOS DE DOMICILIACIONES",
			"dependencias": ["TDRR332_DOMICILIA ", " TDRR347_DOMICILIA_TR ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR855_MP_EMITIDA": {
			"cdValidacion": 181,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR855_MP_EMITIDA",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A TARJETAS EMITIDAS",
			"dependencias": ["TDRR348_MP_EMITIDA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR856_MP_UTILIZADA": {
			"cdValidacion": 182,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR856_MP_UTILIZADA",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A TARJETAS UTILIZADAS",
			"dependencias": ["TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR857_OPERA_TJTA": {
			"cdValidacion": 183,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR857_OPERA_TJTA",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A LAS OPERACIONES REALIZADAS CON TARJETAS",
			"dependencias": ["TDRR333_OPER_ATM ", " TDRR348_MP_EMITIDA ", "TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR858_MP_COMERCIO": {
			"cdValidacion": 184,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR858_MP_COMERCIO",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A COMERCIOS",
			"dependencias": ["TDRR350_MP_COMERCIO"]
		},
		"TDRR859_CU_REL_TJTA": {
			"cdValidacion": 185,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR859_CU_REL_TJTA",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A CUENTAS RELACIONADAS A TARJETAS UTILIZADAS EN EL TRIMESTRE",
			"dependencias": ["TDRR348_MP_EMITIDA ", "TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR860_LMT_CRED_TDC": {
			"cdValidacion": 186,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR860_LMT_CRED_TDC",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A CUENTAS RELACIONADAS A TARJETAS UTILIZADAS EN EL TRIMESTRE",
			"dependencias": ["TDRR348_MP_EMITIDA ", "TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR861_MP_CLIENTE": {
			"cdValidacion": 187,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR861_MP_CLIENTE",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES A CLIENTES Y PAGO DE CLIENTES",
			"dependencias": ["TDRR348_MP_EMITIDA ", "TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR862_PAGO_TJTA": {
			"cdValidacion": 188,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR862_PAGO_TJTA",
			"txComentarios": "CONTIENE LOS INDICADORES CORRESPONDIENTES PAGO DE TARJETAS DE CRÉDITO DE CLIENTES EN OTROS BANCOS",
			"dependencias": ["TDRR348_MP_EMITIDA ", "TDRR349_MP_UTILIZADA ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE"]
		},
		"TDRR863_LOG_AST_USU": {
			"cdValidacion": 189,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR863_LOG_AST_USU",
			"txComentarios": "Carga de información ODS de LOGAST",
			"dependencias": ["TDRR364_USU_AST ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE ", " TDRR554_USU_AST"]
		},
		"TDRR864_LOG_AST_FUN": {
			"cdValidacion": 190,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR864_LOG_AST_FUN",
			"txComentarios": "Carga de información ODS de LOGAST",
			"dependencias": ["TDRR363_LOG_AST ", " TDRR863_LOG_AST_USU ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE ", " TDRR555_FUN_AST"]
		},
		"TDRR865_CUENTA_VISTA": {
			"cdValidacion": 191,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR865_CUENTA_VISTA",
			"txComentarios": "Carga de información ODS de Cuentas Vista",
			"dependencias": ["TDRR353_CUENTA_VISTA", " TDRR359_TALONARIO ", " TDRR354_CUENTA_NOMIN ", " TDRR275_CAPTACION ", " TDRR247_CLIENTE "]
		},
		"TDRR866_CHEQUE": {
			"cdValidacion": 192,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR866_CHEQUE",
			"txComentarios": "Carga de información ODS de Cheques MXN y USD",
			"dependencias": ["TDRR360_EMI_CHQ_MXN ", " TDRR361_CHQ_OBANCO"]
		},
		"TDRR867_DEV_CHEQUE": {
			"cdValidacion": 193,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR867_DEV_CHEQUE",
			"txComentarios": "Carga de información ODS de Devolución Cheques MXN y USD",
			"dependencias": ["TDRR361_CHQ_OBANCO "]
		},
		"TDRR868_TRANS_SWIFT": {
			"cdValidacion": 194,
			"nbEsquema": "FACT",
			"nbTabla": "TDRR868_TRANS_SWIFT",
			"txComentarios": "Carga la información del Modelo Transferencias via Swift",
			"dependencias": ["TDRR367_TRANS_SWIFT"]
		},
		"TDRR182_DOMICILIA": {
			"cdValidacion": 229,
			"nbEsquema": "STG",
			"nbTabla": "TDRR182_DOMICILIA",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["OGTCHIS.txt"]
		},
		"TDRR107_OPER_ATM": {
			"cdValidacion": 230,
			"nbEsquema": "STG",
			"nbTabla": "TDRR107_OPER_ATM",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPOPCAJ.txt"]
		},
		"TDRR184_MP_TARJ_EMI": {
			"cdValidacion": 231,
			"nbEsquema": "STG",
			"nbTabla": "TDRR184_MP_TARJ_EMI",
			"txComentarios": "Carga la informacion acumulada de las tarjetas emitidas de STG",
			"dependencias": ["SSPTEMITIDAS.txt"]
		},
		"TDRR186_MP_TARJ_UTIL": {
			"cdValidacion": 232,
			"nbEsquema": "STG",
			"nbTabla": "TDRR186_MP_TARJ_UTIL",
			"txComentarios": "Carga la información acumulada de las tarjetas utilizadas de STG",
			"dependencias": ["SSPTUTILIZADAS.txt"]
		},
		"TDRR193_COMERCIO": {
			"cdValidacion": 233,
			"nbEsquema": "STG",
			"nbTabla": "TDRR193_COMERCIO",
			"txComentarios": "Carga la información acumulada de los Comercios de STG",
			"dependencias": ["SSPCOMERCIOS.txt"]
		},
		"TDRR191_CUENTA_VISTA": {
			"cdValidacion": 234,
			"nbEsquema": "STG",
			"nbTabla": "TDRR191_CUENTA_VISTA",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPCENITCHEQ"]
		},
		"TDRR192_CUENTA_NOM": {
			"cdValidacion": 235,
			"nbEsquema": "STG",
			"nbTabla": "TDRR192_CUENTA_NOM",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPNOMIN.txt"]
		},
		"TDRR197_TALONARIO": {
			"cdValidacion": 236,
			"nbEsquema": "STG",
			"nbTabla": "TDRR197_TALONARIO",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPTLN.txt"]
		},
		"TDRR701_EMI_CHQ_MXN": {
			"cdValidacion": 237,
			"nbEsquema": "STG",
			"nbTabla": "TDRR701_EMI_CHQ_MXN",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPCHMXN.txt"]
		},
		"TDRR702_EMI_CHQ_USD": {
			"cdValidacion": 238,
			"nbEsquema": "STG",
			"nbTabla": "TDRR702_EMI_CHQ_USD",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPCHUSD.txt"]
		},
		"TDRR704_DEV_CHQ_USD": {
			"cdValidacion": 239,
			"nbEsquema": "STG",
			"nbTabla": "TDRR704_DEV_CHQ_USD",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPCHUSD.txt"]
		},
		"TDRR703_DEV_CHQ_MXN": {
			"cdValidacion": 240,
			"nbEsquema": "STG",
			"nbTabla": "TDRR703_DEV_CHQ_MXN",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPDCHMXN.txt"]
		},
		"TDRR199_USU_AST": {
			"cdValidacion": 241,
			"nbEsquema": "STG",
			"nbTabla": "TDRR199_USU_AST",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPUSUARIOSAST.txt"]
		},
		"TDRR200_FUN_AST": {
			"cdValidacion": 242,
			"nbEsquema": "STG",
			"nbTabla": "TDRR200_FUN_AST",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPCATFUNAST.txt"]
		},
		"TDRR198_LOG_AST": {
			"cdValidacion": 243,
			"nbEsquema": "STG",
			"nbTabla": "TDRR198_LOG_AST",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SSPLOGAST.txt"]
		},
		"TDRR708_TRANS_SWIFT": {
			"cdValidacion": 244,
			"nbEsquema": "STG",
			"nbTabla": "TDRR708_TRANS_SWIFT",
			"txComentarios": "Corte mensual\n Esta tabla no guarda históricos",
			"dependencias": ["SWIFT_SISPAGOS_FXXXXXX.TXT"]
		},
		"TDRR282_GEOLOCALIZA": {
			"cdValidacion": 245,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR282_GEOLOCALIZA",
			"txComentarios": "Tabla existente en DM",
			"dependencias": []
		},
		"TDRR347_DOMICILIA_TR": {
			"cdValidacion": 246,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR347_DOMICILIA_TR",
			"txComentarios": "Esta tabla guarda un histórico de 6 fotos",
			"dependencias": []
		},
		"TDRR275_CAPTACION": {
			"cdValidacion": 247,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR275_CAPTACION",
			"txComentarios": "Tabla existente en DM, se obtiene el nivel de cuenta",
			"dependencias": []
		},
		"TDRR247_CLIENTE": {
			"cdValidacion": 248,
			"nbEsquema": "ODS",
			"nbTabla": "TDRR247_CLIENTE",
			"txComentarios": "Tabla existente en DM, se obtiene la personalidad jurídica",
			"dependencias": []
		},
		"TDRR554_USU_AST": {
			"cdValidacion": 250,
			"nbEsquema": "CONFIG",
			"nbTabla": "TDRR554_USU_AST",
			"txComentarios": "Carga de información USU_AST esquema CONFIG",
			"dependencias": []
		},
		"TDRR555_FUN_AST": {
			"cdValidacion": 251,
			"nbEsquema": "CONFIG",
			"nbTabla": "TDRR555_FUN_AST",
			"txComentarios": "Carga de información FUN_AST esquema CONFIG",
			"dependencias": []
		}
	},
	"estados": {
		"AG": {
			"edo": "MX-AGU",
			"name": "Aguascalientes",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 1,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 33,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 65,
				"NÚMERO TOTAL DE TPVS": 99
			}
		},
		"BN": {
			"edo": "MX-BCN",
			"name": "Baja California Norte",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 2,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 34,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 66,
				"NÚMERO TOTAL DE TPVS": 100
			}
		},
		"BS": {
			"edo": "MX-BCS",
			"name": "Baja California Sur",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 3,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 35,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 67,
				"NÚMERO TOTAL DE TPVS": 101
			}
		},
		"CA": {
			"edo": "MX-CAM",
			"name": "Campeche",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 4,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 36,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 68,
				"NÚMERO TOTAL DE TPVS": 102
			}
		},
		"CS": {
			"edo": "MX-CHP",
			"name": "Chiapas",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 7,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 39,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 71,
				"NÚMERO TOTAL DE TPVS": 105
			}
		},
		"CU": {
			"edo": "MX-CHH",
			"name": "Chihuahua",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 8,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 40,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 72,
				"NÚMERO TOTAL DE TPVS": 106
			}
		},
		"CH": {
			"edo": "MX-COA",
			"name": "Cohahulia de Zaragoza",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 5,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 37,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 69,
				"NÚMERO TOTAL DE TPVS": 103
			}
		},
		"CO": {
			"edo": "MX-COL",
			"name": "Colima",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 6,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 38,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 70,
				"NÚMERO TOTAL DE TPVS": 104
			}
		},
		"DF": {
			"edo": "MX-DIF",
			"name": "Distrito Federal",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 9,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 41,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 73,
				"NÚMERO TOTAL DE TPVS": 107
			}
		},
		"DU": {
			"edo": "MX-DUR",
			"name": "Durango",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 10,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 42,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 74,
				"NÚMERO TOTAL DE TPVS": 108
			}
		},
		"EM": {
			"edo": "MX-MEX",
			"name": "Estado de Mexico",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 11,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 43,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 75,
				"NÚMERO TOTAL DE TPVS": 109
			}
		},
		"GU": {
			"edo": "MX-GUA",
			"name": "Guanajuato",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 13,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 45,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 77,
				"NÚMERO TOTAL DE TPVS": 111
			}
		},
		"GO": {
			"edo": "MX-GRO",
			"name": "Guerrero",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 12,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 44,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 76,
				"NÚMERO TOTAL DE TPVS": 110
			}
		},
		"HI": {
			"edo": "MX-HID",
			"name": "Hidalgo",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 14,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 46,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 78,
				"NÚMERO TOTAL DE TPVS": 112
			}
		},
		"JA": {
			"edo": "MX-JAL",
			"name": "Jalisco",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 15,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 47,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 79,
				"NÚMERO TOTAL DE TPVS": 113
			}
		},
		"MI": {
			"edo": "MX-MIC",
			"name": "Michoacan de Ocampo",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 16,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 48,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 80,
				"NÚMERO TOTAL DE TPVS": 114
			}
		},
		"MO": {
			"edo": "MX-MOR",
			"name": "Morelos",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 17,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 49,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 81,
				"NÚMERO TOTAL DE TPVS": 115
			}
		},
		"NA": {
			"edo": "MX-NAY",
			"name": "Nayarit",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 18,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 50,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 82,
				"NÚMERO TOTAL DE TPVS": 116
			}
		},
		"NL": {
			"edo": "MX-NLE",
			"name": "Nuevo Leon",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 19,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 51,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 83,
				"NÚMERO TOTAL DE TPVS": 117
			}
		},
		"OA": {
			"edo": "MX-OAX",
			"name": "Oaxaca",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 20,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 52,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 84,
				"NÚMERO TOTAL DE TPVS": 118
			}
		},
		"PU": {
			"edo": "MX-PUE",
			"name": "Puebla",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 21,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 53,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 85,
				"NÚMERO TOTAL DE TPVS": 119
			}
		},
		"QR": {
			"edo": "MX-QUE",
			"name": "Queretaro de Arteaga",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 22,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 54,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 86,
				"NÚMERO TOTAL DE TPVS": 120
			}
		},
		"QU": {
			"edo": "MX-ROO",
			"name": "Quintana Roo",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 23,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 55,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 87,
				"NÚMERO TOTAL DE TPVS": 121
			}
		},
		"SL": {
			"edo": "MX-SLP",
			"name": "San Luis Potosi",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 25,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 57,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 89,
				"NÚMERO TOTAL DE TPVS": 123
			}
		},
		"SI": {
			"edo": "MX-SIN",
			"name": "Sinaloa",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 24,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 56,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 88,
				"NÚMERO TOTAL DE TPVS": 122
			}
		},
		"SO": {
			"edo": "MX-SON",
			"name": "Sonora",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 26,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 58,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 90,
				"NÚMERO TOTAL DE TPVS": 124
			}
		},
		"TA": {
			"edo": "MX-TAB",
			"name": "Tabasco",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 27,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 59,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 91,
				"NÚMERO TOTAL DE TPVS": 125
			}
		},
		"TM": {
			"edo": "MX-TAM",
			"name": "Tamaulipas",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 27,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 59,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 91,
				"NÚMERO TOTAL DE TPVS": 127
			}
		},
		"TL": {
			"edo": "MX-TLA",
			"name": "Tlaxcala",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 28,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 60,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 92,
				"NÚMERO TOTAL DE TPVS": 126
			}
		},
		"VE": {
			"edo": "MX-VER",
			"name": "Veracruz",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 30,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 62,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 94,
				"NÚMERO TOTAL DE TPVS": 128
			}
		},
		"YU": {
			"edo": "MX-YUC",
			"name": "Yucatan",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 31,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 63,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 95,
				"NÚMERO TOTAL DE TPVS": 129
			}
		},
		"ZA": {
			"edo": "MX-ZAC",
			"name": "Zacatecas",
			"indicadores": {
				"NÚMERO TOTAL DE CAJEROS REMOTOS": 32,
				"NÚMERO TOTAL DE CAJEROS POR SUCURSAL": 64,
				"TOTAL DE NUMERO DE CAJEROS EN ESTADO": 96,
				"NÚMERO TOTAL DE TPVS": 130
			}
		}
	},
	"diccionario":{
		"1": {
		"cdDiccionario": 1,
		"nbEsquema": "ODS",
		"nbTabla": "TDRR332_DOMICILIA",
		"nbDescipcion": "Contiene la información para Domiciliaciones"
	},
	"2": {
		"cdDiccionario": 2,
		"nbEsquema": "FACT",
		"nbTabla": "TDRR333_OPER_ATM",
		"nbDescipcion": "Contiene la información para Operaciones en Cajeros en ODS"
	},
	"3": {
		"cdDiccionario": 3,
		"nbEsquema": "ODS",
		"nbTabla": "TDRR348_MP_EMITIDA",
		"nbDescipcion": "Carga la informacion acumulada de las tarjetas emitidas"
	},
	"4": {
		"cdDiccionario": 4,
		"nbEsquema": "ODS",
		"nbTabla": "TDRR349_MP_UTILIZADA",
		"nbDescipcion": "Carga la información acumulada de las tarjetas utilizadas"
	},
	"5": {
		"cdDiccionario": 5,
		"nbEsquema": "ODS",
		"nbTabla": "TDRR350_MP_COMERCIO",
		"nbDescipcion": "Carga la información acumulada de los Comercios"
	}
	}
}
