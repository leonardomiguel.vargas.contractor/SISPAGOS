var arrUsuarios = [];

function insertarUsuario(){
  $('#add_usuario').on('shown.bs.modal', function() {
    $("#formUsuario").bootstrapValidator('resetForm',true);
  });
  $('#add_usuario').modal('show');
}

function guardarUsuario(){
  var objUsuarios = {};
  var cdUsuario = $('#cdUsuario').val();
  var nbUsuario = $('#nbUsuario').val();
  var tpPerfil  = $('#tpPerfil').val();
  var nbCorreo  = $('#nbCorreo').val();
  var stUsuario = $('#stUsuario').val();

  var keyUsuario = nbCorreo.split('@')[0].replace(/[.]/g,"");
  objUsuarios = {
    'cdUsuario':cdUsuario,
    'nbUsuario':nbUsuario,
    'tpPerfil' :tpPerfil,
    'nbCorreo' :nbCorreo,
    'stUsuario':stUsuario
  };
  DB.usuarios[keyUsuario] = objUsuarios;
  setUsuario(keyUsuario,objUsuarios);
}

function setUsuario(keyUsuario,row){
  $("body").addClass("loading");
  database.ref('usuarios/'+keyUsuario+'/').set(row);
  $("body").removeClass("loading");
  loadUsuarios();
}

function toggle(value,row,index){
    var options = "";
    var checked = "";
    if(value == 1){
      checked = "checked";
    }
    options ='<ul class="nav nav-pills">'
    +'<li><button type="button" class="btn btn-danger" style="margin: 0px -20px 0px 10px;" onClick="modalDeleteUsuario(\''+row.nbCorreo+'\',\''+row.cdUsuario+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></li>';
    +'</ul>'
    return options;
}

$('#tableUsuarios').bootstrapTable({
  search: true,
  pagination: true,
  pageSize: 10,
  pageList: [10, 15],
  mobileResponsive:true,
  data : arrUsuarios,
  columns: [{
    field: 'cdUsuario',
    title: 'Usuario',
    sortable: true
  },{
    field: 'nbCorreo',
    title: 'Correo',
    sortable: true
  },{
    field: 'nbUsuario',
    title: 'Nombre',
    editable:{type:'text'},
    sortable: true
  },{
    field: 'tpPerfil',
    title: 'Perfil',
    editable:{type:'select',source:[{value:'ADM', text:'ADMINISTRADOR'},{value:'CON', text:'CONSULTOR'},{value: 'OPE', text: 'OPERADOR'}]},
    sortable: true
  },{
    field: 'stUsuario',
    title: 'Estatus',
    sortable: true,
    editable:{type:'select',source:[{value: 1, text: 'HABILITADO'},{value:0, text:'INHABILITADO'}]}
  },{
    field:'stEliminar',
    title : '',
    sortable: false,
    formatter:toggle
  }],
  uniqueId: 'cdUsuario',

  formatShowingRows: function (pageFrom, pageTo, totalRows) {
    return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
  },
  formatRecordsPerPage: function (pageNumber) {
  	return pageNumber + ' registros por pagina';
  },
  formatLoadingMessage: function () {
  	return 'Cargando, espere por favor...';
  },
  formatSearch: function () {
  	return 'Buscar';
  },
  formatNoMatches: function () {
  	return 'No se encontro informaci&oacute;n';
  }
});

$("#manto .search").addClass("input-group");
$("#btnInsertarUsuario").attr('disabled','disabled');
$("#formUsuario").bootstrapValidator({

  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    cdUsuario: {
        validators: {
          notEmpty: {
            message: 'El campo de Usuario de Red es requerido.'
          }
        }
    },
    nbUsuario: {
        validators: {
          notEmpty: {
            message: 'El campo Nombre de Usuario es requerido.'
          }
        }
    },
    tpPerfil: {
        validators: {
          notEmpty: {
            message: 'El campo Perfilado es requerido.'
          }
        }
    },
    nbCorreo: {
        validators: {
          notEmpty: {
            message: 'El campo Correo es requerido.'
          },
          emailAddress: {
            message: 'El correo electrónico no es válido.'
          }
        }
    },
    stUsuario: {
        validators: {
          notEmpty: {
            message: 'El campo Estatus es requerido.'
          }
        }
    }
    }
}).on('status.field.bv', function(e, data) {
  if(!$("#formUsuario").data('bootstrapValidator').isValid()){
    $("#btnInsertarUsuario").attr('disabled','disabled');
  }else{
    $("#btnInsertarUsuario").removeAttr('disabled');
  }
});

function formatterTpPerfil(value,row,index){
    var tpPerfil = 'Administrador';
    if (value == 'OPE') {
      tpPerfil = 'Operador';
    }else if (value == 'CON') {
      tpPerfil = 'Consultor';
    }
    return tpPerfil;
}

function formatterStUsuario(value,row,index){
    var stUsuario = 'Inhabilitado';
    if(value == 1){
      stUsuario = 'Habilitado';
    }
    return stUsuario;
}

$('#tableUsuarios').on('editable-save.bs.table', function(e,field,row){
  var keyUsuario = row.nbCorreo.split('@')[0].replace(/[.]/g,"");
  setUsuario(keyUsuario,row);
});

var cdUsuarioSeleccionado = null;
var keyUsuarioDelete = null;
function modalDeleteUsuario(nbCorreo,cdUsuario){
  var keyUsuario = nbCorreo.split('@')[0].replace(/[.]/g,"");
  keyUsuarioDelete = keyUsuario;
  cdUsuarioSeleccionado = cdUsuario;
  $('#dialogo_eliminar_usuario').modal('show');
}

function eliminarUsuario() {
  delete DB.usuarios[keyUsuarioDelete];
  setUsuario(keyUsuarioDelete,{});
}

function loadUsuarios(){
  arrUsuarios = [];
  $.each(DB.usuarios, function (index, value) {
    arrUsuarios.push(value);
  });
  $("#tableUsuarios").bootstrapTable('load', arrUsuarios);
}

_loadUsuarios(loadUsuarios);
