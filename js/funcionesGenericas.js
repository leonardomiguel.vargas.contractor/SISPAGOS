function _loadPeriodos(rutina){
  $("body").addClass("loading");
  parametria.orderByKey().limitToLast(3).once('value')
            .then(function(snapshot) {
              snapshot.forEach(function(childSnapshot) {
                DB.parametriaDB[childSnapshot.key] = {};
              });
              rutina();
            })
            .catch(err => $("body").removeClass("loading"));
}

function _loadDiccionario(rutina){
  $("body").addClass("loading");
  diccionario.once('value')
            .then(function(snapshot) {
              DB.diccionario = snapshot.val();
              rutina();
              $("body").removeClass("loading");
            })
            .catch(err => $("body").removeClass("loading"));
}

function _loadPeriodo(periodo,rutina){
  $("body").addClass("loading");
  parametria.orderByKey().equalTo(periodo).once('value')
            .then(function(snapshot) {
              DB.parametriaDB[periodo] = snapshot.val()[periodo];
              rutina();
              $("body").removeClass("loading");
            })
            .catch(err => $("body").removeClass("loading"));
}

function loadSISPAGOS(html){
  $('#loadContainer').empty();
  $('#loadContainer').load(html);
}

function loadSISPAGOS_DB(){
  $("body").addClass("loading");
  storageRef.child('sispagos-71dd9-export.txt').getDownloadURL().then(function(url) {
    var xhr = new XMLHttpRequest();
   xhr.responseType = 'text';
   xhr.onload = function(event) {
     DB.parametria = JSON.parse(xhr.responseText);
     $("body").removeClass("loading");
   };
   xhr.open('GET', url, true);
   xhr.send(null);

  }).catch(function(error) {
    console.log(error);
    $("body").removeClass("loading");
  });
}

function publicarBD(){
  var json = {};
  $("body").addClass("loading");
  parametria.orderByKey().limitToLast(6).once('value')
            .then(function(snapshot) {
              json = snapshot.toJSON();
              var numPeriodos = Object.keys(json).length;
              var indice = 0;
              $.each(json, function (keyPeriodo, objPeriodo) {
                var periodoAnioAnterior = (parseInt(keyPeriodo.substring(0,4))-1)+keyPeriodo.substring(4);
                parametria.orderByKey().equalTo(periodoAnioAnterior).once('value')
                          .then(function(snapshot) {
                              json[periodoAnioAnterior] = snapshot.toJSON()[periodoAnioAnterior];
                              indice++;
                              if(numPeriodos == indice){
                                crearFile(json);
                              }
                          })
                          .catch(function(error){
                            indice++;
                            if(numPeriodos == indice){
                              crearFile(json);
                            }
                            console.log(error);
                          });
               });
            })
            .catch(function(error){
              console.log(error);
              $("body").removeClass("loading");
            });


}

function crearFile(json){
  storageRef.child('sispagos-71dd9-export.txt').putString(JSON.stringify(json))
            .then(function(snapshot) {
              $("body").removeClass("loading");
              $("#alertDialog").modal("show");
            })
            .catch(function(){
              $("body").removeClass("loading");
            });
}

function validarCorreo(email,objUsuario) {
  var isValid = false;
  $("#opParametria").hide();
  $("#btnPublicar").hide();
  $("#opManto").hide();
  if(objUsuario != null){
    if(objUsuario.stUsuario == 1){
      isValid = true;
      if(objUsuario.tpPerfil == "OPE"){
        $("#opParametria").show();
      }
      if(objUsuario.tpPerfil == "ADM"){
        $("#opParametria").show();
        $("#opManto").show();
        $("#btnPublicar").show();
      }
    }
  }
  return isValid;
}

function _getUsuario(email, rutina){
  var  keyUsuario = email.split('@')[0].replace(/[.]/g,"");
  var usuario = null;
  $("body").addClass("loading");
  usuarios.orderByKey().equalTo(keyUsuario).once('value')
            .then(function(snapshot) {
              usuario = snapshot.val();
              rutina(usuario[keyUsuario]);
              $("body").removeClass("loading");
            })
            .catch(err => $("body").removeClass("loading"));
}

function _loadUsuarios(rutina){
  $("body").addClass("loading");
  usuarios.once('value')
            .then(function(snapshot) {
              DB.usuarios = snapshot.val();
              rutina();
              $("body").removeClass("loading");
            })
            .catch(err => $("body").removeClass("loading"));
}





function modalPublicarBD(){
  $('#dialogo_publicar_bd').modal('show');
}

function getPeriodoQuery(periodo, nuevoPeriodo){
	var objPeriodo = DB.parametriaDB[periodo];
  $.each(objPeriodo, function (keyValidacion, objValidacion) {
    if (objValidacion != undefined) {
      var txSqlQuery = objValidacion.txSqlQuery;
      var cambioQuery = txSqlQuery.replace(periodo, nuevoPeriodo);
      objPeriodo[keyValidacion].txSqlQuery = cambioQuery;
      objPeriodo[keyValidacion].fhCierre = nuevoPeriodo;
    }
  });
	return objPeriodo;
}
