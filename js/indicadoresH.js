


$('#btnBusquedaAvanzada').hide();
$('#cmbIndicadores').hide();
$.each(DB.secciones, function (keyGroup, objGroup) {

    var isGroup = false;
    var optgroup = $('<optgroup label="'+keyGroup+'">');
    var option = "";

    $.each(objGroup, function (key, obj) {
      if(key != 'rfp'){
          isGroup = true;
          if(obj.length > 0){
            optgroup.append('<option value="'+key+'" data-indicadores="'+obj+'">'+key+'</option>');
          }else{
            optgroup.append('<option value="'+key+'">'+key+'</option>');
          }
      }else{
          option = '<option value="'+keyGroup+'">'+keyGroup+'</option>';
      }
    });

    if(isGroup){
      $('#cmbSeccionesConceptos').append(optgroup);
      isGroup = false;
    }else{
        $('#cmbSeccionesConceptos').append(option);
        option = "";
    }

});

function selectSeccionConcepto(){
   var seccionConcepto = $('#cmbSeccionesConceptos').val();
   if(seccionConcepto != "-1"){
    var lstIndicadores = $('#cmbSeccionesConceptos').find(":selected").attr('data-indicadores') == undefined?"":$('#cmbSeccionesConceptos').find(":selected").attr('data-indicadores');
    var indicadores = lstIndicadores.split(',');
    $('#cmbIndicadores').empty();
    $('#cmbIndicadores').append('<option value="-1" hidden>indicadores</option>');
    if(indicadores.length > 0){
      $.each(indicadores, function (index, value) {
        var indicador = value.split('|');
        if(indicador.length == 2){
          $('#cmbIndicadores').append('<option value="'+indicador[1]+'">'+indicador[0]+'</option>');
        }else{
          $('#cmbIndicadores').append('<option value="'+value+'">'+value+'</option>');
        }

      });
    }
    validIndicador(seccionConcepto,false);
  }else{
    $('#contenedorIndicadores').empty();
    $('#btnBusquedaAvanzada').hide();
    $('#cmbIndicadores').hide();
  }
}

function selectIndicador(){
  validIndicador($('#cmbSeccionesConceptos').val(),true);
}

function validIndicador(seccion,showContenedor){
  switch(seccion){
    case 'DISTRIBUCIÓN DE CAJEROS':
    case 'DISTRIBUCIÓN TPVS':
        $('#contenedorIndicadores').empty();
        $('#btnBusquedaAvanzada').hide();
        $('#cmbIndicadores').show();
        if(showContenedor){
          $('#btnBusquedaAvanzada').show();
          $('#contenedorIndicadores').load("indicadoresMapa.html");
        }
    break;
    case 'DOMICILIACIONES':
      $('#contenedorIndicadores').empty();
      $('#btnBusquedaAvanzada').hide();
      $('#cmbIndicadores').hide();
      $( '#contenedorIndicadores').load( "indicadoresDomiciliacion.html", function() {
        resultadosDomiciliacion(131,145);
      });
    break;
    case 'MEDIOS DE PAGO':
    case 'TRANSFERENCIAS SWIFT':
    case 'BANCA ELECTRÓNICA':
    case 'CUENTAS A LA VISTA NIVEL 4 EN M.N.':
        $('#contenedorIndicadores').empty();
        $('#btnBusquedaAvanzada').hide();
        $('#cmbIndicadores').show();
        if(showContenedor){
          var indicador = $('#cmbIndicadores').val();
          var arrIndicadores = indicador.split('_');
          if(arrIndicadores.length > 1){
            $( '#contenedorIndicadores').load( "indicadoresDomiciliacion.html", function() {
            resultadosDomiciliacion(arrIndicadores[0],arrIndicadores[1]);
            });
          }else {
            $('#contenedorIndicadores').load("indicadoresGrafica.html");
          }
        }
    break;
    default:
       $('#contenedorIndicadores').empty();
       $('#btnBusquedaAvanzada').hide();
    break;
  }
}

if (sessionStorage.INDICADOR_HOME != ' ' && sessionStorage.INDICADOR_HOME != undefined) {
   $('#cmbSeccionesConceptos').val(sessionStorage.INDICADOR_HOME);
   selectSeccionConcepto();
   $('#cmbSeccionesConceptos').attr('disabled', 'disabled');
}else {
  alert('error');
}
